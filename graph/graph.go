package graph

import (
	"sort"

	"../coordparser"
	"../grid"
)

// ------------ DATA STRUCTURES -------------- //

// x -> column
// y -> dimension
type Node struct {
	coords coordparser.Coordinates
	free   bool
}

type Graph struct {
	pins       []*Node
	outputs    []*Node
	emptyNodes []*Node

	board []*Node

	dimension int
}

// ------------ CONSTRUCTORS -------------- //

// if outputCoors = nil, then no outputs are specified
func New(pinCoords []coordparser.Coordinates, outputCoords []coordparser.Coordinates, dimension int) Graph {

	grp := Graph{[]*Node{}, []*Node{}, []*Node{}, []*Node{}, int(dimension)}

	// Create Nodes for pins
	grp.pins = grp.createNodesForPins(pinCoords)

	// Create output Nodes (if given)
	if len(outputCoords) > 0 {
		grp.outputs = grp.createNodesForOutputs(outputCoords)
	}

	// Create Nodes for all the empty vertices (no pins)
	for x := int(0); x <= grp.dimension+1; x++ {
		for y := int(0); y <= grp.dimension+1; y++ {

			if grp.isPin(x, y) {
				continue
			}

			grp.emptyNodes = append(grp.emptyNodes, &(Node{coordparser.New(x, y), true}))

		}
	}

	// Create the board
	grp.addPinsToBoard(&grp.board)

	if len(grp.outputs) != 0 {
		grp.addOutputsToBoard(&grp.board)
	}

	grp.addEmptyNodesToBoard(&grp.board)
	sortBoardByCoords(&grp.board)

	return grp

}

// ------------ ALGORITHMS -------------- //

func (grp Graph) EnumerateForNoOutputs() map[*Node]int {

	m := make(map[*Node]int)

	for _, vertex := range grp.board {

		x := vertex.coords.GetX()
		y := vertex.coords.GetY()

		m[vertex] = min(x, y, (grp.dimension+1)-y, (grp.dimension+1)-x)
	}

	return m

}

func (grp Graph) EnumerateForOutput(output *Node) map[*Node]int {

	m := make(map[*Node]int)

	for _, vertex := range grp.board {

		x := vertex.coords.GetX()
		y := vertex.coords.GetY()

		m[vertex] = abs(output.coords.GetX()-x) + abs(output.coords.GetY()-y)
	}

	return m

}

func (grp Graph) ChoosePathForPin(pin *Node, enumeration map[*Node]int) []*Node {

	path := []*Node{}
	currentPin := pin

	path = append(path, currentPin)

	for i := 1; i <= max(grp.dimension+1, grp.dimension+1); i++ {
		potentialNodes := grp.createVertexPathArray(currentPin.coords.GetX(), currentPin.coords.GetY())

		sort.Slice(potentialNodes, func(i, j int) bool {
			return (enumeration[potentialNodes[i]] < enumeration[potentialNodes[j]])
		})

		if len(potentialNodes) == 0 {
			break
		}

		path = append(path, potentialNodes[0])
		potentialNodes[0].free = false

		if enumeration[potentialNodes[0]] == 0 {
			return path
		}

		currentPin = potentialNodes[0]

	}

	// The path hasn't been found
	// Free all the vertices and return an empty slice

	grp.deletePathArray(path)

	return []*Node{}

}

func (grp Graph) ChoosePathsForAllPins() map[*Node][]*Node {

	m := make(map[*Node][]*Node)

	if len(grp.outputs) == 0 {
		enumeration := grp.EnumerateForNoOutputs()

		for _, pin := range grp.pins {
			m[pin] = grp.ChoosePathForPin(pin, enumeration)
		}

		return m
	} else {

		for index, pin := range grp.pins {
			enumeration := grp.EnumerateForOutput(grp.outputs[index])
			m[pin] = grp.ChoosePathForPin(pin, enumeration)
		}
		return m
	}

}

// ------------ DRAWING METHODS -------------- //

func (grp Graph) DrawGraph(grd grid.Grid) {

	for _, pin := range grp.pins {
		grd.DrawPin(pin.coords.GetX(), pin.coords.GetY())
	}

	if len(grp.outputs) != 0 {
		for _, output := range grp.outputs {
			grd.DrawPin(output.coords.GetX(), output.coords.GetY())
		}
	}

	for _, Node := range grp.emptyNodes {
		grd.DrawEmptyNode(Node.coords.GetX(), Node.coords.GetY())
	}

}

func (grp Graph) DrawPath(grd grid.Grid, Nodes []*Node) {

	coords := []coordparser.Coordinates{}

	for _, Node := range Nodes {
		coords = append(coords, Node.coords)
	}

	grd.DrawPath(coords)
}

func (grp Graph) DrawAllPaths(grd grid.Grid) {

	m := grp.ChoosePathsForAllPins()

	for _, pin := range grp.pins {
		grp.DrawPath(grd, m[pin])
	}

}

// ------------ HELPER METHODS -------------- //

func sortByCoords(pinCoords *([]coordparser.Coordinates)) {
	sort.Slice(*pinCoords, func(i, j int) bool {
		if (*pinCoords)[i].GetX() < (*pinCoords)[j].GetX() {
			return true
		}
		if (*pinCoords)[i].GetX() > (*pinCoords)[j].GetX() {
			return false
		}
		return (*pinCoords)[i].GetY() < (*pinCoords)[j].GetY()
	})
}

func (grp Graph) iterateOverBoard(f func(*Graph, int, int)) {
	for x := int(0); x <= grp.dimension+1; x++ {
		for y := int(0); y <= grp.dimension+1; y++ {
			f(&grp, x, y)
		}
	}
}

func (grp Graph) createNodesForPins(pinCoords []coordparser.Coordinates) []*Node {

	for _, pinCoord := range pinCoords {
		grp.pins = append(grp.pins, &(Node{coordparser.New(pinCoord.GetX(), pinCoord.GetY()), false}))
	}

	return grp.pins
}

func (grp Graph) createNodesForOutputs(outputCoords []coordparser.Coordinates) []*Node {

	for _, outputCoord := range outputCoords {
		grp.outputs = append(grp.outputs, &(Node{coordparser.New(outputCoord.GetX(), outputCoord.GetY()), true}))
	}

	return grp.outputs
}

func (grp Graph) isPin(col int, row int) bool {

	var x, y int

	for _, pin := range grp.pins {
		x = pin.coords.GetX()
		y = pin.coords.GetY()

		if x == col && y == row {
			return true
		}
	}

	// If there are outputs, check them too:
	if grp.outputs != nil {
		for _, output := range grp.outputs {
			x = output.coords.GetX()
			y = output.coords.GetY()

			if x == col && y == row {
				return true
			}
		}
	}

	return false

}

func (grp Graph) addPinsToBoard(board *([]*Node)) {

	for _, pin := range grp.pins {
		*board = append(*board, pin)
	}

}

func (grp Graph) addOutputsToBoard(board *([]*Node)) {

	for _, output := range grp.outputs {
		*board = append(*board, output)
	}

}

func (grp Graph) addEmptyNodesToBoard(board *([]*Node)) {

	for _, Node := range grp.emptyNodes {
		*board = append(*board, Node)
	}

}

func (grp Graph) isInBounds(col, row int) bool {
	return col >= 0 && row >= 0 && col <= grp.dimension+1 && row <= grp.dimension+1
}

func (grp Graph) createVertexPathArray(col, row int) []*Node {

	result := []*Node{}

	grp.addVertexToPathArray(&result, col+1, row)
	grp.addVertexToPathArray(&result, col-1, row)
	grp.addVertexToPathArray(&result, col, row+1)
	grp.addVertexToPathArray(&result, col, row-1)

	return result
}

func (grp Graph) addVertexToPathArray(pathArray *([]*Node), col int, row int) {

	if grp.isInBounds(col, row) && grp.IsFree(col, row) {
		*pathArray = append(*pathArray, grp.GetNode(col, row))
	}

}

func (grp Graph) deletePathArray(pathArray []*Node) {

	for index, vertex := range pathArray {

		// Don't free the pin
		if index != 0 {
			vertex.free = true
		}
	}

}

func sortBoardByCoords(board *([]*Node)) {
	sort.Slice(*board, func(i, j int) bool {

		n1 := (*board)[i].coords
		n2 := (*board)[j].coords

		if n1.GetX() < n2.GetX() {
			return true
		}
		if n1.GetX() > n2.GetX() {
			return false
		}
		return n1.GetY() < n2.GetY()
	})
}

func min(i, j, k, l int) int {
	a := []int{i, j, k, l}

	sort.Ints(a)

	return a[0]
}

func max(i, j int) int {
	if i > j {
		return i
	} else {
		return j
	}
}

func abs(i int) int {
	if i < 0 {
		return -i
	}

	return i
}

// ------------ GETTERS --------------------- //

func (grp Graph) IsFree(col int, row int) bool {
	return grp.board[row+col*(grp.dimension+2)].free
}

func (grp Graph) GetNode(col int, row int) *Node {
	return grp.board[row+col*(grp.dimension+2)]
}

func (grp Graph) GetPins() []*Node {
	return grp.pins
}

func (grp Graph) GetOutputs() []*Node {
	return grp.outputs
}

func (grp Graph) GetEmptyNodes() []*Node {
	return grp.emptyNodes
}

func (n Node) GetCoords() coordparser.Coordinates {
	return n.coords
}
