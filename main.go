package main

import (
	"flag"
	"log"
	"time"

	"./coordparser"
	"./flowgraph"
	"./generator"
	"./graph"
	"./grid"
	"./window"
	"github.com/faiface/pixel/pixelgl"
)

const title string = "Pinmapping"
const width int = 1000
const height int = 1000

func run() {

	mode := flag.String("mode", "enum", "Mode of execution: enum, output or flow")
	generation := flag.Bool("gen", false, "Automatic data generation enabled")
	dimension := flag.Int("dim", 3, "Dimension of the microcontroller board")

	flag.Parse()

	coords := []coordparser.Coordinates{}
	outputs := []coordparser.Coordinates{}

	if *generation {
		coords = generator.GenerateRandomCoords(*dimension)
	} else {
		println("Pins: [x y] [empty line terminates input loop]")
		coords = coordparser.ReadCoordsPair()
		validateCoords(*dimension, coords)

	}

	if *mode == "output" {
		if *generation {
			outputs = generator.GenerateRandomCoords(*dimension)
		} else {
			println("Outputs: [x y] [empty line terminates input loop]")
			outputs = coordparser.ReadCoordsPair()
			validateOutputCoords(*dimension, outputs, coords)
		}
	}

	win := window.New(title, width, height)
	grd := grid.New(width, height, *dimension)

	// Start of execution time measurement
	start := time.Now()

	switch *mode {
	case "enum":
		{
			grp := graph.New(coords, []coordparser.Coordinates{}, *dimension)
			grd.DrawGrid()
			grp.DrawGraph(grd)
			grp.DrawAllPaths(grd)
			break
		}
	case "output":
		{
			grp := graph.New(coords, outputs, *dimension)
			grd.DrawGrid()
			grp.DrawGraph(grd)
			grp.DrawAllPaths(grd)
			break
		}
	case "flow":
		{
			fg := flowgraph.New(coords, *dimension)
			grd.DrawGrid()
			fg.DrawFlowGraph(grd)
			break
		}
	}

	// End of time measurement
	elapsed := time.Since(start)
	log.Printf("Elapsed time: %s", elapsed)

	win.Display(grd.Display)
}

func main() {

	pixelgl.Run(run)
}

func validateCoords(dimension int, coords []coordparser.Coordinates) {

	for _, coord := range coords {
		x := coord.GetX()
		y := coord.GetY()

		if x == 0 || y == 0 || x > dimension || y > dimension {
			panic("Coordinates aren't on the microcontroller board")
		}
	}

}

func validateOutputCoords(dimension int, outputCoords []coordparser.Coordinates, coords []coordparser.Coordinates) {

	if len(outputCoords) != len(coords) {
		panic("Lists of outputs and pins have to be of the same length")
	}

	for _, coord := range outputCoords {
		x := coord.GetX()
		y := coord.GetY()

		if (x != 0 && x != dimension+1) && (y != 0 && y != dimension+1) {
			panic("Coordinates must be on the microcontroller board edge")
		}
	}

}
