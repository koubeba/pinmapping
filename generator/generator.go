package generator

import (
	"math/rand"
	"time"

	"../coordparser"
)

func GenerateRandomCoords(dim int) []coordparser.Coordinates {

	result := []coordparser.Coordinates{}
	rand.Seed(time.Now().UnixNano())

	for len(result) < dim {
		coord := coordparser.New(rand.Intn(dim)+1, rand.Intn(dim)+1)
		if !isInResult(result, coord) {
			result = append(result, coord)
		}
	}

	return result

}

func isInResult(coords []coordparser.Coordinates, coord coordparser.Coordinates) bool {

	for _, c := range coords {
		if coord.GetX() == c.GetX() && coord.GetY() == c.GetY() {
			return true
		}
	}

	return false

}
