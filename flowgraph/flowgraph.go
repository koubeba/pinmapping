package flowgraph

import (
	"sort"

	"../coordparser"
	"../graph"
	"../grid"
)

type Node struct {
	coords coordparser.Coordinates
	from   *([]*edge)
}

type edge struct {
	from       *Node
	to         *Node
	returnEdge *edge
	capacity   int
	flow       int
}

type residualEdge struct {
	edge     *edge
	residual int
}

type FlowGraph struct {
	source *Node
	sink   *Node

	grp   *graph.Graph
	edges *[]*edge
	nodes *[]*Node

	dimension int
}

// --------------- CONSTRUCTORS --------------------- //

func New(pinCoords []coordparser.Coordinates, dimension int) FlowGraph {

	fgrp := FlowGraph{nil, nil, nil, &([]*edge{}), &([]*Node{}), dimension}

	source := Node{coordparser.New(-1, -1), &([]*edge{})}
	sink := Node{coordparser.New(dimension+2, dimension+2), &([]*edge{})}

	fgrp.source = &source
	fgrp.sink = &sink

	// Helper graph
	grp := graph.New(pinCoords, []coordparser.Coordinates{}, dimension)
	fgrp.grp = &grp

	// Add edges from the source to the pins
	for _, pin := range fgrp.grp.GetPins() {
		pinNode := createNode(pin)
		*fgrp.nodes = append(*fgrp.nodes, pinNode)
		fgrp.AddOneWayEdge(&source, pinNode)
	}

	// for _, output := range fgrp.grp.GetOutputs() {
	// 	node := createNode(output)
	// 	*fgrp.nodes = append(*fgrp.nodes, node)
	// }

	for _, e := range fgrp.grp.GetEmptyNodes() {
		node := createNode(e)
		*fgrp.nodes = append(*fgrp.nodes, node)
	}

	sortBoardByCoords(fgrp.nodes)

	// Add edges from the output vertices to the sink

	for i := 0; i <= dimension+1; i++ {
		// From the leftmost
		fgrp.AddOneWayEdge(fgrp.getNode(0, i), &sink)

		// From the rightmost
		fgrp.AddOneWayEdge(fgrp.getNode(dimension+1, i), &sink)
	}

	for i := 0; i <= dimension+1; i++ {
		// From the topmost
		fgrp.AddOneWayEdge(fgrp.getNode(i, 0), &sink)

		fgrp.AddOneWayEdge(fgrp.getNode(i, dimension+1), &sink)
	}

	// Add edges
	for row := 0; row <= dimension+1; row++ {
		for col := 0; col <= dimension+1; col++ {

			if row != 0 && col != 0 && col != dimension+1 {
				if fgrp.isPin(fgrp.getNode(row-1, col)) && fgrp.isPin(fgrp.getNode(row, col)) {
					continue
				}
				fgrp.AddEdge(fgrp.getNode(row-1, col), fgrp.getNode(row, col))
			}

			if col != 0 && row != 0 && row != dimension+1 {
				if fgrp.isPin(fgrp.getNode(row, col-1)) && fgrp.isPin(fgrp.getNode(row, col)) {
					continue
				}
				fgrp.AddEdge(fgrp.getNode(row, col-1), fgrp.getNode(row, col))
			}

		}
	}

	// And replace the edges
	for _, pin := range *fgrp.nodes {

		pin2 := &(Node{coordparser.New(pin.coords.GetX(), pin.coords.GetY()), &([]*edge{})})
		pin2.from = pin.from

		for _, edge := range *pin2.from {
			edge.from = pin2
		}

		pin.from = &([]*edge{})

		pinToPinEdge := edge{pin, pin2, nil, 1, 0}

		*pin.from = append(*pin.from, &pinToPinEdge)

	}

	return fgrp
}

func (grp FlowGraph) GetFlowPath(start *Node, end *Node, path *([]*residualEdge), hasPin bool) *([]*residualEdge) {

	// We have reached the end (in the flow graph case: the sink)
	if start == end {
		return path
	}

	for _, edge := range *start.from {

		residual := edge.capacity - edge.flow

		if residual > 0 && !isEdgeInPath(residualEdge{edge, residual}, path) {

			*path = append(*path, &(residualEdge{edge, residual}))
			result := grp.GetFlowPath(edge.to, end, path, hasPin)
			if result != nil {
				return result
			}
		}
	}

	return nil

}

func (fgrph FlowGraph) FlowAlgorithm() *[][]*residualEdge {

	result := &([][]*residualEdge{})

	path := fgrph.GetFlowPath(fgrph.source, fgrph.sink, &([]*residualEdge{}), false)
	if path != nil {
		*result = append(*result, *path)
	}
	for path != nil {
		for _, rEdge := range *path {
			rEdge.edge.flow++
			if rEdge.edge.returnEdge != nil {
				rEdge.edge.returnEdge.flow--
			}
		}
		path = fgrph.GetFlowPath(fgrph.source, fgrph.sink, &([]*residualEdge{}), false)
		if path != nil && (*path)[len(*path)-1].edge.to == fgrph.sink {
			*result = append(*result, *path)
		}
	}

	return result

}

func (fgrph FlowGraph) GetPathsInFlow() []*edge {

	edges := &([]*edge{})

	for _, edge := range *fgrph.edges {
		if edge.flow == 1 {
			*edges = append(*edges, edge)
		}
	}

	return *edges

}

func (fgrph FlowGraph) preparePath(path []*residualEdge) *[]*edge {

	result := &([]*edge{})

	if path[len(path)-1].edge.to != fgrph.sink {
		return &([]*edge{})
	}

	// Index of source
	srcIndex := 0

	for index, rE := range path {
		if rE.edge.from == fgrph.source {
			srcIndex = index
		}
	}

	for i := srcIndex; i < len(path); i++ {
		*result = append(*result, path[i].edge)
	}

	return result
}

func (fgrp FlowGraph) DrawFlowGraph(grd grid.Grid) {

	fgrp.grp.DrawGraph(grd)

	for _, edge := range *fgrp.edges {
		grd.DrawEdge(edge.from.coords.GetX(), edge.from.coords.GetY(), edge.to.coords.GetX(), edge.to.coords.GetY())
	}

	for _, path := range *fgrp.FlowAlgorithm() {
		for _, edge := range *(fgrp.preparePath(path)) {
			grd.DrawPathEdge(edge.from.coords.GetX(), edge.from.coords.GetY(), edge.to.coords.GetX(), edge.to.coords.GetY())
		}
	}

}

func (fgrp FlowGraph) isSource(Node *Node) bool {
	return Node == fgrp.source
}

func isEdgeInPath(edge residualEdge, path *([]*residualEdge)) bool {

	for _, rE := range *path {
		if rE.edge == edge.edge && rE.residual == edge.residual {
			return true
		}
	}

	return false
}

func getEdgeWithMinFlow(edges []*residualEdge) *residualEdge {

	if len(edges) == 0 {
		return nil
	}

	sort.Slice(edges, func(i, j int) bool {
		return edges[i].edge.flow < edges[j].edge.flow
	})

	return edges[0]
}

func createNode(n *graph.Node) *Node {
	return &(Node{n.GetCoords(), &([]*edge{})})
}

func (fgrp FlowGraph) AddOneWayEdge(from *Node, to *Node) {

	newEdge := &(edge{from, to, nil, 1, 0})

	*fgrp.edges = append(*fgrp.edges, newEdge)
	*from.from = append(*from.from, newEdge)

}

func (fgrp FlowGraph) AddEdge(from *Node, to *Node) {

	fgrp.AddSingleEdge(from, to)
	fgrp.AddSingleEdge(to, from)

}

func (fgrp FlowGraph) AddSingleEdge(from *Node, to *Node) {

	newEdge := &(edge{from, to, nil, 1, 0})
	returnEdge := &(edge{to, from, nil, 0, 0})
	newEdge.returnEdge = returnEdge
	returnEdge.returnEdge = newEdge

	*fgrp.edges = append(*fgrp.edges, newEdge, returnEdge)
	*from.from = append(*from.from, newEdge)
	*to.from = append(*to.from, returnEdge)

}

func (fgrp FlowGraph) isPin(node *Node) bool {

	for _, pin := range fgrp.grp.GetPins() {
		if node.coords.GetX() == pin.GetCoords().GetX() && node.coords.GetY() == pin.GetCoords().GetY() {
			return true
		}
	}

	return false

}

// -------------- HELPER METHODS --------------------- //

func sortBoardByCoords(board *([]*Node)) {
	sort.Slice(*board, func(i, j int) bool {

		n1 := (*board)[i].coords
		n2 := (*board)[j].coords

		if n1.GetX() < n2.GetX() {
			return true
		}
		if n1.GetX() > n2.GetX() {
			return false
		}
		return n1.GetY() < n2.GetY()
	})
}

// ------------- GETTERS ------------------------- //

func (fgrp FlowGraph) getNode(col int, row int) *Node {
	return (*fgrp.nodes)[row+col*(fgrp.dimension+2)]
}
