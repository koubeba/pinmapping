Maja Jabłońska
286320
AAL

1. AAL.11- Układ ścieżek w mikrokontrolerze

Zadanie polega na ustaleniu ścieżki z zewnątrz dla każdego pinu na powierzchni przekroju przez warstwę mikrokontrolera.

2. Uruchomienie programu

Program napisany jest w języku Go.
Na początku należy go skompilować poleceniem `go build` wykonanym w folderze $GOPATH.
Następnie może być uruchomiony poleceniem `go run main.go` z możliwymi parametrami:
	--mode ["enum", "output", "flow"]- tryb wykonania programu
	--dim [int]- wymiar przekroju

3. Dane wejściowe

Dane pobierane są w postaci par koordynatów x, y, gdzie każda para musi być w osobnej linii.
Najpierw zostają pobrane koordynaty pinów. Aby zakończyć wprowadzanie, należy wprowadzić jedną pustą linijkę.

Jeżeli został wybrany tryb "output" (ustalone wyjścia), po pobraniu danych pinów zostają przyjęte dane wyjść w ten sam sposób.
Pierwszemu pinowi odpowiada pierwsze wprowadzone wyjście, drugiemu- drugie, itd.


4. Prezentacja wyników

Efektem działania programu jest wyświetlenie obrazu z zaznaczonymi pinami i wyznaczonymi przez algorytm dojściami z zewnątrz.

5. Moduły programu

W ramach programu powstały następujące moduły:
	coordparser: wprowadzanie, parsowanie i walidacja danych wejściowych
	flowgraph: algorytm przepływu i reprezentacja grafu przepływu
	graph: algorytm numeracji i wyznaczania najkrótszych ścieżek, reprezentacja tablicy pinów
	grid: wyświetlanie grafiki
	window: wyświetlanie okna


