package window

import (
	"github.com/faiface/pixel"
	"github.com/faiface/pixel/pixelgl"
	"golang.org/x/image/colornames"
)

type Window struct {
	// Window configuration
	title  string
	height int
	width  int

	// Window pointer
	WindowPointer *pixelgl.Window
}

func New(title string, height int, width int) Window {

	// Create Window configuration object from pixelgl library:
	WindowConfig := pixelgl.WindowConfig{
		Title:  title,
		Bounds: pixel.R(0, 0, float64(width), float64(height)),
		VSync:  true,
	}

	// Create Window object from pixelgl library
	win, err := pixelgl.NewWindow(WindowConfig)

	// Check if any errors were thrown
	if err != nil {
		panic(err)
	}

	wToReturn := Window{title, height, width, win}

	return wToReturn
}

func (win Window) Display(df func(*pixelgl.Window)) {

	//win.WindowPointer.Clear(colornames.White)

	for !win.WindowPointer.Closed() {
		win.WindowPointer.Clear(colornames.White)
		df(win.WindowPointer)
		win.WindowPointer.Update()
	}

}
