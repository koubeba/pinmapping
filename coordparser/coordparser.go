package coordparser

import (
	"bufio"
	"os"
	"regexp"
	"strconv"
	"strings"
)

const numberRegex string = `^\d+ \d+$`

// Coordinates

type Coordinates struct {
	x int
	y int
}

func New(x int, y int) Coordinates {
	return Coordinates{x, y}
}

func (c Coordinates) GetX() int {
	return c.x
}

func (c Coordinates) GetY() int {
	return c.y
}

// String validation

func validateString(input string) bool {

	re := regexp.MustCompile(numberRegex)
	return re.MatchString(input)

}

func parseStringToCoords(input string) (x int, y int) {

	coords := strings.Fields(input)
	x, errx := strconv.Atoi(coords[0])
	y, erry := strconv.Atoi(coords[1])

	if errx != nil || erry != nil {
		// handle error
	}

	return x, y
}

func ReadCoordsPair() []Coordinates {
	// We are going to read data from console
	scanner := bufio.NewScanner(os.Stdin)

	// Store the Coordinates
	vec := make([]Coordinates, 0)

	// Reading line by line
	for scanner.Scan() {
		if validateString(scanner.Text()) {
			x, y := parseStringToCoords(scanner.Text())
			// Do anything with x and y
			vec = append(vec, Coordinates{x, y})
		} else {
			break
		}
	}

	if scanner.Err() != nil {
		// handle error.
	}

	return vec
}
