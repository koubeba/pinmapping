package grid

import (
	"../coordparser"
	"github.com/faiface/pixel"
	"github.com/faiface/pixel/imdraw"
	"github.com/faiface/pixel/pixelgl"
	"golang.org/x/image/colornames"
)

// ------------ DATA STRUCTURES -------------- //

// x -> dimension
// y -> dimension
type Grid struct {
	width     int // Width of the window in px
	height    int // Height of the window in px
	dimension int // Microcontroller board dimenstion

	// The grid is going to have +2 dimension and dimension: the additional
	// ones are the ones that are the outputs

	imd    *imdraw.IMDraw
	margin int // Margin left on the sides of grid: used only to draw

	rowGap    int // Gap between lines: used only to draw
	columnGap int
}

// ------------ CONSTRUCTOR -------------- //

func New(width int, height int, dimension int) Grid {

	grd := Grid{width, height, dimension, imdraw.New(nil), int(250), int(0), int(0)}

	// There are going to be lines-1 gaps
	// and we need to calculate their width
	// (also considering the margins)
	grd.rowGap = (grd.height - 2*grd.margin) / (grd.dimension + 1)
	grd.columnGap = (grd.width - 2*grd.margin) / (grd.dimension + 1)

	return grd

}

// ------------ SINGLE COMPONENTS -------------- //

func (grd Grid) drawHorizontalLine(row int, beginPoint int, endPoint int) {

	grd.imd.Color = colornames.Blueviolet
	grd.imd.EndShape = imdraw.SharpEndShape
	grd.imd.Push(pixel.V(float64(beginPoint), float64(row)), pixel.V(float64(endPoint), float64(row)))
	grd.imd.EndShape = imdraw.SharpEndShape
	grd.imd.Line(5)

}

func (grd Grid) drawVerticalLine(column int, beginPoint int, endPoint int) {

	grd.imd.Color = colornames.Blueviolet
	grd.imd.EndShape = imdraw.SharpEndShape
	grd.imd.Push(pixel.V(float64(column), float64(beginPoint)), pixel.V(float64(column), float64(endPoint)))
	grd.imd.EndShape = imdraw.SharpEndShape
	grd.imd.Line(5)

}

func (grd Grid) pushPixel(xPoint int, yPoint int) {
	grd.imd.Push(pixel.V(float64(grd.margin+xPoint*grd.columnGap), float64(grd.margin+yPoint*grd.rowGap)))
}

// ------------ PUBLIC METHODS -------------- //

func (grd Grid) DrawPoint(xPoint int, yPoint int) {

	grd.imd.Color = colornames.Red
	grd.pushPixel(xPoint, yPoint)
	grd.imd.Circle(5, 0)

}

func (grd Grid) DrawPin(xPoint int, yPoint int) {

	grd.imd.Color = colornames.Plum
	grd.pushPixel(xPoint, yPoint)
	grd.imd.Circle(10, 0)

}

func (grd Grid) DrawEmptyNode(xPoint int, yPoint int) {

	grd.imd.Color = colornames.Greenyellow
	grd.pushPixel(xPoint, yPoint)
	grd.imd.Circle(5, 0)

}

func (grd Grid) DrawNonEmptyNode(xPoint int, yPoint int) {

	grd.imd.Color = colornames.Hotpink
	grd.pushPixel(xPoint, yPoint)
	grd.imd.Circle(5, 0)

}

func (grd Grid) DrawPath(coords []coordparser.Coordinates) {

	grd.imd.Color = colornames.Coral
	grd.imd.EndShape = imdraw.SharpEndShape

	for _, coord := range coords {
		grd.pushPixel(coord.GetX(), coord.GetY())
	}

	grd.imd.EndShape = imdraw.SharpEndShape
	grd.imd.Line(3)

}

func (grd Grid) DrawEdge(xPoint1 int, yPoint1 int, xPoint2 int, yPoint2 int) {

	grd.imd.Color = colornames.Mistyrose
	grd.imd.EndShape = imdraw.SharpEndShape
	grd.pushPixel(xPoint1, yPoint1)
	grd.pushPixel(xPoint2, yPoint2)
	grd.imd.EndShape = imdraw.SharpEndShape
	grd.imd.Line(2)

}

func (grd Grid) DrawPathEdge(xPoint1 int, yPoint1 int, xPoint2 int, yPoint2 int) {

	grd.imd.Color = colornames.Tomato
	grd.imd.EndShape = imdraw.SharpEndShape
	grd.pushPixel(xPoint1, yPoint1)
	grd.pushPixel(xPoint2, yPoint2)
	grd.imd.EndShape = imdraw.SharpEndShape
	grd.imd.Line(5)

}

func (grd Grid) DrawGrid() {
	for i := 0; i <= int(grd.dimension+1); i++ {
		grd.drawVerticalLine(grd.margin+int(i*int(grd.columnGap)), grd.margin, grd.width-grd.margin)
	}

	for i := 0; i <= int(grd.dimension+1); i++ {
		grd.drawHorizontalLine(grd.margin+int(i*int(grd.rowGap)), grd.margin, grd.height-grd.margin)
	}
}

func (grd Grid) DrawCoords(coords []coordparser.Coordinates) {

	for _, coord := range coords {
		grd.DrawPoint(coord.GetX(), coord.GetY())
	}

}

func (grd Grid) Display(win *pixelgl.Window) {

	grd.imd.Draw(win)

}

func (grd Grid) Getdimension() int {

	return grd.dimension

}
